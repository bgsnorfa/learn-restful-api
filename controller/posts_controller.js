let posts = require("../data/posts");

exports.getPosts = function(req, res) {
  let user = res.user;
  let userPosts = posts.filter(p => p.user_id === user.id);
  res.json(userPosts);
}

exports.createPost = function(req, res) {
  let user = res.user;
  let body = req.body;
  let title = body.title;
  let content = body.content;

  let data = {
    id: posts.length + 1,
    user_id: user.id,
    title: title,
    content: content
  };

  posts.push(data);

  res.json({
    message: "Posts created!",
    data: data
  });
}

exports.editPost = function(req, res) {
  let user = res.user;
  let id = req.params.id;
  let body = req.body;
  let title = body.title;
  let content = body.content;

  let post = posts.find(p => p.id == id);
  if (post && post.user_id === user.id) {
    post.title = title;
    post.content = content;

    posts.map(p => p.id === post.id ? post : p);

    res.json({
      message: "Post updated",
      data: post
    });
  } else {
    res.status(404).json({
      message: "post not found"
    });
  }
}

exports.deletePost = function(req, res) {
  let user = res.user;
  let id = req.params.id;

  let post = posts.find(p => p.id == id);
  if (post && post.user_id === user.id) {
    posts = posts.filter(p => p.id != id);

    res.json({
      message: "post deleted"
    });
  } else {
    res.status(404).json({
      message: "post not found"
    });
  }
}
