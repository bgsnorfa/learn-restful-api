let posts = [
  { id: 1, user_id: 1, title: "Post pertama", content: "Hello World"},
  { id: 2, user_id: 1, title: "Post kedua", content: "Hello World again"},
  { id: 3, user_id: 1, title: "Post ketiga", content: "Hello World again and again"},
  { id: 4, user_id: 2, title: "Post Baru", content: "Hello World dunia"},
];

module.exports = posts;