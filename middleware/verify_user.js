let users = require("../data/users");

exports.verifyUser = function(req, res, next) {
  let auth = req.headers['authorization'];
  let user = users.find(u => u.token === auth);
  if (user) {
    res.user = user;
    next();
  } else {
    res.status(401).json({
      message: "unauthorized"
    });
  }
}