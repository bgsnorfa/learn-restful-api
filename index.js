const express = require("express");
const bodyParser = require("body-parser");

const jsonParser = bodyParser.json();

const app = express();

const port = "3000";

const {getPosts, createPost, editPost, deletePost} = require("./controller/posts_controller");
const {verifyUser} = require("./middleware/verify_user");

// Read
app.get("/posts", jsonParser, verifyUser, getPosts);

// Create
app.post("/posts", jsonParser, verifyUser, createPost)

// Update
app.patch("/post/:id", jsonParser, verifyUser, editPost);

// Delete
app.delete("/post/:id", jsonParser, verifyUser, deletePost)

app.listen(port, () => {
  console.log(`App listening to http://localhost:${port}`);
});